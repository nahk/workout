<?php
require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

// Kernel
$app
    ->register(new Silex\Provider\UrlGeneratorServiceProvider())
    ->register(new Silex\Provider\TwigServiceProvider(), array(
        'twig.path' => __DIR__.'/views',
    ))
;

// Twig
$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
    $twig->addExtension(new \Entea\Twig\Extension\AssetExtension($app));
    
    return $twig;
}));

// Controllers & Routes
$app->get('/', function () use ($app) {
    return $app['twig']->render('abchallenge.html.twig');
})->bind('index');

$app->get('/abchallenge', function () use ($app) {
    return $app['twig']->render('abchallenge.html.twig');
})->bind('abchallenge');

/*
$app->get('/hello/{name}', function ($name) use ($app) {
    return $app['twig']->render('hello.html.twig', array(
        'name' => $name,
    ));
})->bind('hello');
*/

// Run
$app->run();