Workout
=======

Active workouts
---------------

* [30-day AB challenge](http://workout.clmnt.me/)

Installation
------------

Retrieves the project from the server

    git clone git@bitbucket.org:nahk/workout.git

Adding the production repository :

    git remote add prod ssh+git://773965@git.dc0.gpaas.net/workout.clmnt.me.git

Then, you need composer. Download it if you do not have a global installation :

    curl -sS https://getcomposer.org/installer | php

Look if he's installed :

    php composer.phar

And then install the project with `composer.phar install` :

    php composer.phar install

Deployment
----------

If it's your first deployment, you should clean all unversionned files on the server :

    ssh 773965@git.dc0.gpaas.net 'clean workout.clmnt.me.git'

To begin, you have to push your local version to the production repository :

    git push prod

Then deploy the code :

    ssh 773965@git.dc0.gpaas.net 'deploy workout.clmnt.me.git'

To deploy a specific branch, for example the `branch` branch, use :

    ssh 773965@git.dc0.gpaas.net 'deploy workout.clmnt.me.git branch'

For the moment, waiting a solution to run a `php composer.phar install|update` command on production